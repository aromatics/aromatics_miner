#What is Aromatics?#
Aromatics is a GPU-only miner for ARO for NVIDIA cards (Cuda, ie GTX 1060, GTX 1070 ...). 
It is a fork of Cryptogone's Arionum-gpu-miner.

#What OS does Aromatics run on?#
Currently tested on:
Ubuntu 18.04 with nvidia drivers 396 (recommended for max performance) or 390

Ubuntu 16.04 with nvidia drivers 390, 396, 410

Windows 10 x64 with nvidia 416.34 (make sure you are running drivers 416.34 or later)

#How to install on Ubuntu 18.04 or 16.04?#

Dependencies:

    sudo apt install aptitude wget libcpprest libboost-all-dev ocl-icd-opencl-dev -y

    #Graphic Driver. Skip this is you already have nvidia drivers 390 or 396 installed
    #check what nvidia version you have
    nvidia-smi

    #if you have version 390 or 396 skip the below
    sudo add-apt-repository ppa:graphics-drivers/ppa -y
    sudo apt update
    sudo aptitude install nvidia-driver-396 -y

    #REBOOT

If you already had an nvidia driver installed and want to upgrade to 396:

    sudo apt purge nvidia*
    sudo add-apt-repository ppa:graphics-drivers/ppa -y
    sudo apt install aptitude -y
    sudo apt update
    sudo aptitude install nvidia-driver-396 -y

if the above fails then try:

    sudo apt-get install nvidia-396

create a folder to put the miner in, and go inside it

    mkdir aromatics
    cd aromatics

download latest archive. 

If you are using Ubuntu 18.04 + nvidia drivers 396 (recommended):
    
    wget -O aromatics.tar.gz "https://bitbucket.org/aromatics/aromatics_miner/downloads/aromatics-v1.3-ubuntu18-nvidia396.tar.gz"

If you are using Ubuntu 18.04 + nvidia drivers 390:
    
    wget -O aromatics.tar.gz "https://bitbucket.org/aromatics/aromatics_miner/downloads/aromatics-v1.3-ubuntu18-nvidia390.tar.gz"

If you are using Ubuntu 16.04 + nvidia drivers 390: *please note fixed version uploaded 31/10/2018*
    
    wget -O aromatics.tar.gz "https://bitbucket.org/aromatics/aromatics_miner/downloads/aromatics-v1.3-ubuntu16-nvidia390-v2.tar.gz"
  
If you are using Ubuntu 16.04 + nvidia drivers 396:
    
    wget -O aromatics.tar.gz "https://bitbucket.org/aromatics/aromatics_miner/downloads/aromatics-v1.3-ubuntu16-nvidia396.tar.gz"
  
If you are using Ubuntu 16.04 + nvidia drivers 410:
    
    wget -O aromatics.tar.gz "https://bitbucket.org/aromatics/aromatics_miner/downloads/aromatics-v1.3-ubuntu16-nvidia410.tar.gz"
  
extract archive (please note that the folder to which the archive is decompressed will change depending on the version you downloaded)
    
    tar xzvf aromatics.tar.gz
    cd aromatics-v1.3-linux-nvidia396


You are now ready to run Aromatics! Follow the instructions below.

#How to install on Windows 10?#

Aromatics will only work on Windows 10 x64 edition, and with nvidia drivers 416 or later installed. A recent release of Windows 10 is required for colors. 

To install Aromatics, simply download the ZIP file in Downloads (left menu of Bitbuckets), extract to a folder on your hard drive and double click on **colors.reg** to have colors in your miner (otherwise the display will not look nice at all). 
Then follow the instructions below.


#Finding the optimal settings for Aromatics#
Note: if you are migrating from Cryptogone's arionum-gpu-miner you can use the same settings. No need to rerun the test module.

You must try to maximise GPU usage. You do so by increasing threads (1,2,3,4) and batches per thread (ie: 50, 100, 200) until you maximised memory usage. 

I find optimal performance working card after card.

List existing cards on your system:
    
    ./aromatics -l

Note card number. (this is the number after the -d switch below)

For A 3 GB Card (GTX 1060)
    
    ./aromatics --test-mode -d 0 -b 90 -t 2

For an 8 GB card (GTX 1070, 1070ti, 1080) start here:
    
    ./aromatics --test-mode -d 0 -b 244 -t 2

For an 11 GB card (GTX 1080ti)
    
    ./aromatics --test-mode -d 0 -b 226 -t 3

If Aromatics gives an error (not enough memory) reduce batch size and repeat until you find the batch size which does not create an error. 
If Aromatics does not give an error, increase batch size until reaching an error and then use batch size just before the error. 

Then once you are at maximum usage for 2 threads, repeat the process for 3 and 4 threads. 

Once you found maximum batches for 2, 3 and 4 threads let the test mode run for a minute at maximum settings, write down hash rates for both GPU and CPU rounds and find if the optimal setting is 2, 3 or 4 threads. 

Repeat for other cards. 

Note: 
- On a rig with identical cards you do not need to repeat this for every card. However the card connected to the monitor will have memory allocated to displaying the screen so less memory will be available for mining. You will need to reduce batch size for this specific card. 
- mining card numbers are not necessarily the same as nvidia-smi (linux) card numbers

#Running Aromatics#

Commandline Options

    -l, --list-devices                list all available devices and exit
    -d, --device(s)=DEVICES           GPU devices to use, examples: -d 0 / -d 1 / -d 0,1,3,5 [default: 0]
    -u, --use-all-devices             use all available GPU devices (overrides -d)
    -a, --address=ADDRESS             public arionum address. This should be your wallet
    -p, --pool=POOL_URL               pool URL [default: http://aropool.com]
    -t, --threads-per-device=THREADS  number of threads to use per device, ex: -t 1 / -t 6,3 [default: 1]
    -b, --batch-per-device=BATCHES    number of batches to use per device, ex: -b 6 / -b 6,3 [default: 48]
    -i, --workerId=WORKER_ID          worker id [default: autogenerated]
    --test-mode                   test CPU/GPU blocks hashrate
    --skip-cpu-blocks             do not mine cpu blocks
    --skip-gpu-blocks             do not mine gpu blocks
    --legacy-5s-hashrate          show 5s avg hashrate instead of last set of batches hashrate
    -?, --help                        show this help and exit

I recommend editing mine.sh / mine.bat included with the miner. 

#Mining Fee#

The mining fee is 2%.

#FAQ#

**It does not work**

Make sure on Ubuntu that you are running a driver version that is compatible with Aromatics:

For Ubuntu 18: version 390 or 396.54

For Ubuntu 16: version 390, 396 or 410. 

To know which driver version you are running type:

    nvidia-smi

Also make sure that you downloaded the version that corresponds to both your Ubuntu version and your driver version.

On Windows make sure you are running drivers 416.34 or later. 
Also Aromatics will only work on x64 systems.

**In Linux I am getting the error" while loading shared libraries: libboost_system.so.1.65.1 is missing..."**

You are probably not running Ubuntu 16.04 or 18.04. As Aromatics is pre-compiled it needs very specific dependencies that are included in Ubuntu 18. Currently only Ubuntu 18 is supported amongst Linux flavors.

**In Windows I do not have colors and my titles have strange characters**

Double click on colors.reg (in your aromatics folder) to enable the color upgrade. This will only work on recent version of Windows 10.

**Why does my AMD RX580 not work?**

This is a CUDA only miner. Nvidia cards only.

**My hash rate seems to fluctuate every few minutes. Sometimes it's like 5 times higher!**

ARO has 2 minings rounds: a GPU round and a CPU round. Hash rates are much higher during the GPU round. 
Both the CPU round and the GPU round can be mined with either a GPU or a CPU.
Do not worry if your hash rate is lower during the CPU round: so is everybody else's.

**How can I tell if it's a GPU or CPU round?**

Your hash rate is a good sign. But also in the title bar above the date you will see a red GPU or CPU. Yeah, if you're over 45 the chances are you will not see the difference between a G and a C without good reading glasses.

**I've been mining but my hash rate does not appear in the pool.**

Some pools only display your hash rate after your first share. 

**How is the mining fee paid?**

For every share you submit 2/100 are sent as a mining fee. 
But every share that you see in your miner is yours. In other words if Aromatics displays 100 shares submitted, that's 100 shares for you. 

**I have different GPUs in a rig: how do I best set them?**

Find the optimal point for each GPU with the test mode (see above) and then use the triggers `-d 0,1,2 -t 2,2,3 -b 50,60,70`
